
FROM debian:buster-slim

ENV TERRAFORM_VERSION=0.14.11

ENTRYPOINT ["/script.sh"]

COPY ./script.sh /
RUN chmod +x /script.sh

RUN apt-get update; apt-get install -y unzip wget
RUN cd /tmp && \
    wget https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_amd64.zip && \
    unzip terraform_${TERRAFORM_VERSION}_linux_amd64.zip -d /usr/bin
RUN rm -rf /tmp/*