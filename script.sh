#!/bin/bash
set -x
rm -rf .terraform
terraform --version
terraform init -backend-config="bucket=u-${CI_ENVIRONMENT_NAME}-terraform-state" 

